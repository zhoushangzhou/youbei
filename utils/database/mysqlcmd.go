package db

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"
	md "youbei/models"
)

func MysqlCmdDump(info *md.Task, dist string) error {
	out, fileerr := os.OpenFile(dist, os.O_CREATE|os.O_RDWR, os.ModePerm|os.ModeTemporary)
	if fileerr != nil {
		return fileerr
	}
	defer out.Close()
	cmds := md.SystemBackupCmdPath{}
	if bol, err := md.Localdb().Where("id=? and status=0", info.Cmds).Get(&cmds); err != nil {
		return err
	} else {
		if !bol {
			return errors.New("dbtype not found2")
		}
	}

	var cmd *exec.Cmd
	var cmdstr string
	var cmsarr = []string{}
	var cmdbase string

	sysos := runtime.GOOS
	if sysos == "windows" {
		cmdbase = "powershell"
		cmdstr = Fmtpath(cmds.Path) + " " + fmt.Sprintf(`--complete-insert --skip-lock-tables --skip-comments --compact --add-drop-table --host %s --port %s -u%s --password=%s --default-character-set %s -B %s`, info.Host, info.Port, info.User, info.Password, info.Char, info.DBname)
		cmsarr = strings.Split(cmdstr, " ")
		cmd = exec.Command(cmdbase, cmsarr...)
	} else {
		cmdstr = fmt.Sprintf(`--complete-insert --skip-lock-tables --skip-comments --compact --add-drop-table --host %s --port %s -u%s --password=%s --default-character-set %s -B %s`, info.Host, info.Port, info.User, info.Password, info.Char, info.DBname)
		cmsarr = strings.Split(cmdstr, " ")
		cmd = exec.Command(Fmtpath(cmds.Path), cmsarr...)
	}

	var stderr bytes.Buffer
	cmd.Stdout = out
	cmd.Stderr = &stderr

	if err := cmd.Run(); err != nil {
		return errors.New(err.Error() + ":" + stderr.String())
	}

	out.Close()

	return nil
}
